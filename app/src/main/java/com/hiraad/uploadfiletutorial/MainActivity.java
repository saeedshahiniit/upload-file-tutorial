package com.hiraad.uploadfiletutorial;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.net.URI;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    View progressBar;
    View uploadFileButton;
    private Retrofit retrofit;
    private static final int PICK_FILE_REQUEST_ID = 1001;
    private ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupRetrofit();
        setupViews();
    }

    private void setupRetrofit() {
        retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://kotlins.ir/")
                .build();

        apiService = retrofit.create(ApiService.class);
    }

    private void setupViews() {
        progressBar = findViewById(R.id.rl_main_progressBarContainer);
        uploadFileButton = findViewById(R.id.btn_main_uploadFile);
        uploadFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("file/*");
                startActivityForResult(intent, PICK_FILE_REQUEST_ID);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_FILE_REQUEST_ID && data != null) {
            File file = new File(URI.create(data.getDataString()));
            Log.i(TAG, "onActivityResult: " + file.getAbsolutePath());
        }
    }
}
